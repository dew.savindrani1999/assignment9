//202079 - K.A.D.S Rathnawardhane

#include <stdio.h>

int text;
int main()
{

    FILE *f1, *f2, *f3;

    f1 = fopen("assignment9.txt", "w");
    fprintf(f1, "UCSC is one of the leading institutes in Sri Lanka for computing studies. ");
    fclose(f1);

    f2 = fopen("assignment9.txt", "r");
    while ((text = getc(f2)) != EOF)
        putchar(text);
    fclose(f2);
    f3 = fopen("assignment9.txt", "a");
    fprintf(f3, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields. ");
    fclose(f3);
    return 0;
}
